
from django.conf.urls import url, include
from django.contrib import admin
from . import appviews
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
	url(r'^$', appviews.home),
    url(r'^admin/', admin.site.urls),
    url(r'^signup/$', appviews.register),
    url(r'^logout/$', appviews.logout_user, name='logout'),
    url(r'^login/$', appviews.login_user, name='login'),
    url(r'^markfavorite/(?P<tweet_id>[0-9]+)/$', appviews.mark_favorite),
    url(r'^makeretweet/(?P<tweet_id>[0-9]+)/$', appviews.make_retweet),
    url(r'^', include('tweetAPI.urls')),

]

urlpatterns += [
    url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework')),
]

if settings.DEBUG:
	urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)