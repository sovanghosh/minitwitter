from django.shortcuts import render,redirect
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib.auth import authenticate, login, logout
from tweetAPI.models import Tweet, Retweet
import json

def register(request):
	return render(request, 'signup.html')


def home(request):
	if not request.user.is_authenticated():
		return render(request, 'signup.html')
	user = request.user
	tweet_count = user.tweets.count()
	following_count = user.following.count()
	follower_count = user.followers.count()
	return render(request, 'dashboard.html', {'tweet_count': tweet_count,
												'following_count' : following_count,
												'follower_count' : follower_count,
												})


def logout_user(request):
	logout(request)
	return redirect('/signup/')


def login_user(request):
	if request.method == "POST":
		username = request.POST["username"]
		password = request.POST["passwd"]
		user = authenticate(username=username, password=password)

		if user is not None:
			login(request, user)
			return redirect('/')
		return render(request, 'signup.html', {'err_msg': 'invalid credentials'})

	return render(request, 'signup.html')

def mark_favorite(request, tweet_id):
	if request.method == "GET":
		if request.user.is_authenticated():
			print tweet_id
			user = request.user
			tweet = None
			try:
				tweet = Tweet.objects.get(id=int(tweet_id))
			except Tweet.DoesNotExist:
				return HttpResponse("tweet does not exist with id "+ str(tweet_id))
			users_marked_favorite = tweet.favorites.all()
			count = users_marked_favorite.count()
			if user in users_marked_favorite:
				tweet.favorites.remove(user)
				count -= 1
			else:
				tweet.favorites.add(user)
				count += 1
			json_data = json.dumps({'count':count})
			return HttpResponse(json_data)
		return render(request, 'dashboard.html')


def make_retweet(request, tweet_id):
	if request.method == 'GET':
		if request.user.is_authenticated():
			user = request.user
			tweet = None
			try:
				tweet = Tweet.objects.get(id=int(tweet_id))
			except Tweet.DoesNotExist:
				return HttpResponse("tweet does not exist with id "+ str(tweet_id))

			r = None
			count = Retweet.objects.filter(tweet=tweet).count()
			try:
				r = Retweet.objects.get(user=user, tweet=tweet)
			except Retweet.DoesNotExist:
				r = None

			if r is not None:
				r.delete()
				count -= 1
			else:
				Retweet.objects.create(user=user, tweet=tweet)
				count += 1
			json_data = json.dumps({'count':count})
			return HttpResponse(json_data)
		return render(request, 'dashboard.html')