from .models import Tweet, TweeterUser, Comment, Retweet
from .serializers import TweetsListSerializer, UserSerializer, TweetDetailSerializer, CommentSerializer, RetweetSerializer
from rest_framework import permissions
from rest_framework import viewsets
from django_filters.rest_framework import DjangoFilterBackend
import filters
from django.db.models import Q


class UserViewSet(viewsets.ModelViewSet):
	queryset = TweeterUser.objects.all()
	serializer_class = UserSerializer
	filter_backends = (DjangoFilterBackend,)
	filter_fields = ('username','email')


class RetweetViewSet(viewsets.ModelViewSet):
	serializer_class = RetweetSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		user = self.request.user
		following_users = list(user.following.all())
		return Retweet.objects.filter(user__in=following_users).order_by('-timestamp')


class TweetViewSet(viewsets.ModelViewSet):
	serializer_class = TweetsListSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		user = self.request.user
		following_users = user.following.values_list('id')
		return Tweet.objects.filter(Q(user__id__in=following_users) | Q(user=user) | Q(retweet__user__id__in=following_users) | Q(retweet__user=user)).distinct().order_by('-timestamp')

	def perform_create(self, serializer):
		serializer.save(user=self.request.user)

class CommentViewSet(viewsets.ModelViewSet):
	serializer_class = CommentSerializer
	permission_classes = (permissions.IsAuthenticated,)
	queryset = Comment.objects.all()

	def perform_create(self, serializer):
		tweet = Tweet.objects.get(id=int(self.request.GET['tweet']))
		serializer.save(user=self.request.user, tweet=tweet)