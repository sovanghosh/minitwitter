from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models.signals import post_save
from django.dispatch import receiver
from datetime import datetime, timedelta


class TweeterUser(AbstractUser):
	followers = models.ManyToManyField('tweetAPI.TweeterUser', related_name='following')
	first_name = models.CharField(max_length=30, blank=False)
	last_name = models.CharField(max_length=30, blank=False)
	email = models.EmailField(unique= True)

	def __unicode__(self):
		return self.username

	def full_name(self):
		return self.first_name + ' ' + self.last_name

class Tweet(models.Model):
	user = models.ForeignKey(TweeterUser, on_delete = models.CASCADE, related_name = 'tweets')
	post = models.CharField(max_length = 140)
	timestamp = models.DateTimeField(auto_now_add = True)
	favorites = models.ManyToManyField(TweeterUser, related_name = 'get_favorites', blank=True)
	# retweets = models.ForeignKey('tweetAPI.Retweet', related_name='original_tweet')

	def __unicode__(self):
		return self.post

class Retweet(models.Model):
	user = models.ForeignKey(TweeterUser, on_delete=models.CASCADE)
	tweet = models.ForeignKey(Tweet, on_delete=models.CASCADE, related_name='retweet')
	timestamp = models.DateTimeField(auto_now_add=True)

	class Meta:
		unique_together = ('user', 'tweet')


class Comment(models.Model):
	tweet = models.ForeignKey(Tweet, related_name='comments')
	user = models.ForeignKey(TweeterUser, related_name='user_comments')
	comment = models.CharField(max_length = 140)

	def __unicode__(self):
		return self.comment

class Hashtag(models.Model):
	hashtag_name = models.CharField(unique=True, max_length = 20)
	tweets = models.ManyToManyField(Tweet, related_name='hastags')
	last_used = models.DateTimeField()
	occurence_last_day = models.IntegerField()

	def __unicode__(self):
		return self.hashtag_name


@receiver(post_save, sender=Tweet)
def signal_handler(sender, **kwargs):
	tweet = kwargs['instance']
	print tweet.timestamp
	post = tweet.post
	hastags = [i for i in post.split(" ") if i.startswith("#")]

	for i in hastags:
		h=None
		try:
			h = Hashtag.objects.get(hashtag_name=i)
		except Hashtag.DoesNotExist:
			print "exception caught"
			new_hashtag = Hashtag.objects.create(hashtag_name=i, last_used=datetime.now(), occurence_last_day=1)
			new_hashtag.tweets.add(tweet)
		if h is not None:
			cur_time = datetime.now()
			_time = cur_time-timedelta(hours=24)
			print h.last_used
			last_used = h.last_used.replace(tzinfo=None)
			if last_used > _time:
				h.occurence_last_day += 1
			else:
				h.occurence_last_day = 1
			h.last_used = cur_time
			h.save()
			h.tweets.add(tweet)