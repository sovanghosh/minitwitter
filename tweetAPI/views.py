from .models import Tweet, TweeterUser, Comment
from .serializers import TweetsListSerializer, UserSerializer, TweetDetailSerializer, CommentSerializer
from rest_framework import generics
from rest_framework import permissions
from django.db.models import Q


class SingleUserTweetList(generics.ListAPIView):
    serializer_class = TweetsListSerializer

    def get_queryset(self):
		username = self.kwargs['username']
		user = TweeterUser.objects.get(username=username)
		return Tweet.objects.filter(user__username=username) | user.get_retweets.all()



class FollowingUsersTweetList(generics.ListAPIView):
	serializer_class = TweetsListSerializer
	permission_classes = (permissions.IsAuthenticated,)

	def get_queryset(self):
		user = self.request.user
		following_users = list(user.following.all())
		return Tweet.objects.filter(Q(user__in=following_users) | Q(user=user)).order_by('-timestamp')

	# def perform_create(self, serializer):
	# 	serializer.save(user=self.request.user)


class TweetDetail(generics.RetrieveAPIView):
	queryset = Tweet.objects.all()
	serializer_class = TweetDetailSerializer
	permission_classes = (permissions.IsAuthenticated,)



class FollowersList(generics.ListAPIView):
	serializer_class = UserSerializer

	def get_queryset(self):
		user = self.request.user
		return user.followers.all()


class FollowingList(generics.ListAPIView):

	serializer_class = UserSerializer
	def get_queryset(self):

		user = self.request.user
		return user.following.all()

class CommentsList(generics.ListAPIView):

	serializer_class = CommentSerializer
	queryset = Comment.objects.all()