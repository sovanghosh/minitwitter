from django.conf.urls import url
from . import views, api
from rest_framework import routers


router = routers.SimpleRouter()
router.register(r'users', api.UserViewSet)
router.register(r'tweets', api.TweetViewSet, 'tweet')
router.register(r'retweets', api.RetweetViewSet, 'retweet')
router.register(r'comments', api.CommentViewSet)

urlpatterns = router.urls
