from django.contrib import admin
from .models import TweeterUser, Tweet, Comment, Hashtag, Retweet


# Register your models here.


admin.site.register(TweeterUser)
admin.site.register(Tweet)
admin.site.register(Comment)
admin.site.register(Hashtag)
admin.site.register(Retweet)
