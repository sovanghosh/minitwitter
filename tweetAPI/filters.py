import django_filters
from .models import Tweet, TweeterUser, Comment


class TweetFilter(django_filters.FilterSet):
	user = django_filters.CharFilter(name='user__username')

	class Meta:
		model = Tweet
		fields = ['user']