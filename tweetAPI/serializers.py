from rest_framework import serializers
from .models import Tweet, TweeterUser, Comment, Retweet
from django.db.models import Q


class UserSerializer(serializers.ModelSerializer):
	password = serializers.CharField(style={'input_type': 'password'}, write_only=True)

	class Meta:
		model = TweeterUser
		fields = ('id', 'username','password', 'email', 'first_name', 'last_name')
		read_only_field = ('id',)

	def create(self, validated_data):
		user = TweeterUser(
			email=validated_data['email'],
			username=validated_data['username'],
			first_name = validated_data['first_name'],
			last_name = validated_data['last_name']
		)
		user.set_password(validated_data['password'])
		user.save()
		return user

class TweetDetailSerializer(serializers.ModelSerializer):
	user = UserSerializer()

	class Meta:
		model = Tweet
		fields = '__all__'


class RetweetSerializer(serializers.ModelSerializer):
	user = UserSerializer()

	class Meta:
		model = Retweet
		fields = ('user', 'timestamp')

class CommentSerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=True)
	tweet = TweetDetailSerializer(read_only=True)

	class Meta:
		model = Comment
		fields = '__all__'


class TweetsListSerializer(serializers.ModelSerializer):
	user = UserSerializer(read_only=True)
	retweet_count = serializers.SerializerMethodField()
	favorite_count = serializers.SerializerMethodField()
	retweets = serializers.SerializerMethodField()
	is_following_tweet = serializers.SerializerMethodField()
	comments = serializers.SerializerMethodField()

	class Meta:
		model = Tweet
		fields = '__all__'
		read_only_field = ('timestamp','favorites', 'retweet_count', 'favorite_count', 'retweets', 'comments')

	def get_retweet_count(self, obj):
		return Retweet.objects.filter(tweet__id=obj.id).count()

	def get_favorite_count(self, obj):
		return obj.favorites.count()

	def get_retweets(self, obj):
		user = self.context["request"].user
		following_users = user.following.values_list('id')
		qs = Retweet.objects.filter(Q(tweet=obj) & (Q(user__id__in=following_users) | Q(user=user))).order_by('timestamp')
		return RetweetSerializer(qs, many=True, read_only=True).data

	def get_is_following_tweet(self, obj):
		user = self.context["request"].user
		following_users = user.following.values_list('id')
		if obj.user.id in following_users:
			return True
		if obj.user.id==user.id:
			return True
		return False

	def get_comments(self, obj):
		qs = Comment.objects.filter(tweet=obj)
		return CommentSerializer(qs, many=True).data


# class CommentDetailSerializer(serializers.ModelSerializer):

# 	parent = serializers.ReadOnlyField(source='parent_comment.id')
# 	replies = serializers.SerializerMethodField()

# 	class Meta:
# 		model = Comment
# 		fields = ('id', 'tweet_id', 'user_id', 'comment', 'parent', 'replies')

# 	def get_replies(self, obj):

# 		replies_qs = obj.replies.all()
# 		return CommentSerializer(replies_qs, many=True).data


