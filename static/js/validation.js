$(document).ready(function(){

	$("#messageId").html("");

	jQuery.validator.addMethod("email", function(value, element) {
 		return this.optional(element) || /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/.test(value);}, 
 		"invalid email"
	);

	isExist = function(field, value){

		var status = false;
		$.ajax({
			type: "GET",
			async: false,
			url: "http://127.0.0.1:8000/users/?" + field + "=" + value,
			success: function(data) {
				if (data.length == 0) status = true; 
			}
		});
		return status;
	};

	jQuery.validator.addMethod("isUsernameExist", function(value, element) {
		return isExist('username', value);
	}, "username already exists");

	jQuery.validator.addMethod("isEmailExist", function(value, element) {
		return isExist('email', value);
	}, "email already exists");	
	
	$('#signupformId').validate({
		
		rules: {
			email:{
				required:true,
				email:true,
				isEmailExist:true
			},
			firstname:{
				required:true,
				nowhitespace:true,
				lettersonly:true
			},
			lastname:{
				required:true,
				lettersonly:true,
				nowhitespace:true
			},
			username:{
				required:true,
				minlength:4,
				isUsernameExist:true
			},
			passwd1:{
				required:true,
				minlength:3
			},
			passwd2:{
				required:true,
				equalTo : "#passwordId1"
			}
		},
		
		submitHandler: function(form) {
			console.log(form);
			var data = {}; 
			$("#signupform form").serializeArray().map(function(x){data[x.name] = x.value;});
			var post_data = {};
			// console.log(data);
			post_data.username = data.username;
			post_data.email = data.email;
			post_data.first_name = data.firstname;
			post_data.last_name = data.lastname;
			post_data["password"] = data.passwd1;
			post_data.csrfmiddlewaretoken = $("input[name=csrfmiddlewaretoken]").val();
			// console.log(post_data);


			
			$.ajax({
				type: "POST",
				url: "http://127.0.0.1:8000/users/",
				data: post_data,
				success: function(data) {
					$("#signupformId")[0].reset();
					$("#messageId").html("<div class='alert alert-success' role='alert'>you have successfully signed up</div>");
					$('#signupform').hide(); 
					$('#loginform').show();
				}
			});
			
			
			
			
		}
	});

	$("#loginformId").validate({

		rules:{
			username:{
				required:true
			},
			passwd:{
				required:true
			}
		}
	});
	
});